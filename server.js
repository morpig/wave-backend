var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var firebase = require("firebase");

var firebaseRef = new firebase("https://dazzling-inferno-5459.firebaseio.com/wave");

var Auth0Client = require('auth0').ManagementClient;

var api = new Auth0Client({
  domain: 'waveid.auth0.com',
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjQWNzRm5RMHpUeVFLYWhjV3JSNEM3UmtqM1BCeGs2UCIsInNjb3BlcyI6eyJ1c2VycyI6eyJhY3Rpb25zIjpbImNyZWF0ZSJdfSwidG9rZW5zIjp7ImFjdGlvbnMiOlsiYmxhY2tsaXN0Il19fSwiaWF0IjoxNDU3ODAxNzA4LCJqdGkiOiJjMjMwZWY5ZTk3Y2YxMzk5ZTJlMjc0YWRhNDFhMjBlOCJ9.MhIxOmIJfgEvwDxnvS7rZ4yhhIlxsYPLyrNu95EhjCo'
});

app.get('/', function(req, res){
  res.sendfile('index.html');
});

io.on('connection', function(socket){
  console.log('a user connected. waiting for authentication.');

  socket.on("authentication", function(json) {
    console.log(json);
    var socketAuth = firebaseRef.child("socketAuth");
    socketAuth.child(json.socketID).set(json, function(error) {
      if (error) {
        socket.emit("authReply", {text: "Failed Authentication." + json.socketID + ", " + err, code: 0});
      } else {
        socket.emit("authReply", {text: "Successfull Authentication." + json.socketID, code: 1});
      }
    });

  });


  socket.on("registerUser", function(json) {
    console.log(json);
    api.createUser(json, function(err) {
      if (err) {
        console.log(err);
        return;
      };
      console.log("SUCCESSSSSS");
    })
  });

  socket.on("getAuth0Data", function() {
    socket.emit("receiveAuth0Data", {domain: 'waveid.auth0.com', clientID: 'mGFNIodU8UMENQHIdVJzWM54IvgHkXx2'})
  })

  socket.on("disconnect", function() {
    console.log(socket.conn.id);
    var socketAuth = firebaseRef.child("socketAuth");
    socketAuth.child(socket.conn.id).remove();
  })
});

http.listen(3001, function(){
  console.log('Hextris running in port 3001');
});
